package com.gitlab.vincenthung.cxf.jaxrs.oauth2.configuration;

import org.apache.cxf.rs.security.oauth2.client.AccessTokenGrantWriter;
import org.apache.cxf.rs.security.oauth2.provider.OAuthJSONProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProviderConfiguration {

	@Bean
	public OAuthJSONProvider oAuthJSONProvider() {
		return new OAuthJSONProvider();
	}

	@Bean
	public AccessTokenGrantWriter accessTokenGrantWriter() {
		return new AccessTokenGrantWriter();
	}

}
