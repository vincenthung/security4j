package com.gitlab.vincenthung.cxf.jaxrs.oauth2.client;

import java.util.Arrays;
import java.util.Base64;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.rs.security.oauth2.client.AccessTokenGrantWriter;
import org.apache.cxf.rs.security.oauth2.common.ClientAccessToken;
import org.apache.cxf.rs.security.oauth2.provider.OAuthJSONProvider;
import org.apache.cxf.rs.security.oauth2.utils.OAuthConstants;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class OAuth2JAXRSClient {

	@Resource
	private OAuthJSONProvider oAuthJSONProvider;

	@Resource
	private AccessTokenGrantWriter accessTokenGrantWriter;

	@Value("${jaxrs.security.oauth2.tokenUrl}")
	private String tokenUrl;

	private WebClient oAuth2Client;

	@PostConstruct
	public void initClient() {
		WebClient oAuth2Client = WebClient.create(tokenUrl,
				Arrays.asList(oAuthJSONProvider, accessTokenGrantWriter));

		this.oAuth2Client = oAuth2Client
				.type(MediaType.APPLICATION_FORM_URLENCODED)
				.accept(MediaType.APPLICATION_JSON);

	}

	public enum AuthMethod {
		CLIENT_SECRET_POST,
		CLIENT_SECRET_BASIC,
		;
	}

	public ClientAccessToken getToken(String clientId, String clientSecret, AuthMethod authMethod) {
		switch (authMethod) {
		case CLIENT_SECRET_BASIC:
			return getTokenClientSecretBasic(clientId, clientSecret);
		case CLIENT_SECRET_POST:
			return getTokenClientSecretPost(clientId, clientSecret);
		}
		return null;

	}

	private ClientAccessToken getTokenClientSecretPost(String clientId, String clientSecret) {

		return oAuth2Client
				.post(Entity.entity(
						new Form()
								.param(OAuthConstants.GRANT_TYPE, OAuthConstants.CLIENT_CREDENTIALS_GRANT)
								.param(OAuthConstants.CLIENT_ID, clientId)
								.param(OAuthConstants.CLIENT_SECRET, clientSecret),
						MediaType.APPLICATION_FORM_URLENCODED_TYPE
						),
						ClientAccessToken.class);

	}

	private ClientAccessToken getTokenClientSecretBasic(String clientId, String clientSecret) {

		String authorizationBasic = clientId + ":" + clientSecret;
		String authBasicHeader = OAuthConstants.BASIC_SCHEME + " " + Base64.getEncoder().encodeToString(
				authorizationBasic.getBytes());

		return oAuth2Client
				.header(HttpHeaders.AUTHORIZATION, authBasicHeader)
				.post(Entity.entity(
						new Form()
								.param(OAuthConstants.GRANT_TYPE, OAuthConstants.CLIENT_CREDENTIALS_GRANT),
						MediaType.APPLICATION_FORM_URLENCODED_TYPE
						),
						ClientAccessToken.class);
	}

}
