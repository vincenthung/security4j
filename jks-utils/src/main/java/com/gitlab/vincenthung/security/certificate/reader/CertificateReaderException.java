package com.gitlab.vincenthung.security.certificate.reader;

import java.text.MessageFormat;

public class CertificateReaderException extends Exception {

	private static final long serialVersionUID = -154147568345892080L;

	public CertificateReaderException() {
		super();
	}

	public CertificateReaderException(Throwable cause) {
		super(cause);
	}

	public CertificateReaderException(String message, Object[] params, Throwable cause) {
		super(MessageFormat.format(message, params), cause);
	}

	public CertificateReaderException(String message, Object... params) {
		this(message, params, null);
	}

	public CertificateReaderException(String message, Throwable cause) {
		this(message, null, cause);
	}

}
