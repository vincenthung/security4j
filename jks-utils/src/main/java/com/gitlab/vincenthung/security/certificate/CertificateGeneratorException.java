package com.gitlab.vincenthung.security.certificate;

import java.text.MessageFormat;

public class CertificateGeneratorException extends Exception {

	private static final long serialVersionUID = 3748484886042920885L;

	public CertificateGeneratorException() {
		super();
	}

	public CertificateGeneratorException(Throwable cause) {
		super(cause);
	}

	public CertificateGeneratorException(String message, Object[] params, Throwable cause) {
		super(MessageFormat.format(message, params), cause);
	}

	public CertificateGeneratorException(String message, Object... params) {
		this(message, params, null);
	}

	public CertificateGeneratorException(String message, Throwable cause) {
		this(message, null, cause);
	}

}
