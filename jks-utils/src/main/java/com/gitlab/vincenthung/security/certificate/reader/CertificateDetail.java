package com.gitlab.vincenthung.security.certificate.reader;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;

import com.gitlab.vincenthung.security.keystore.CertificateType;
import com.gitlab.vincenthung.security.util.CertificateUtils;

import sun.security.x509.X500Name;

public class CertificateDetail {

	public CertificateDetail(byte[] certificate) throws CertificateReaderException {

		ByteArrayInputStream bais = new ByteArrayInputStream(certificate);
		String type = CertificateType.X509.getType();

		try {
			this.certificate = (X509Certificate) CertificateUtils.loadCertificate(bais, type);
		} catch (CertificateException e) {
			throw new CertificateReaderException(
					"Failed to load Certificate with type [{0}]",
					new Object[] {type}, e);
		}

		String subjectName = this.certificate.getSubjectX500Principal().getName();
		try {
			X500Name subject = new X500Name(subjectName);
			this.commonName = subject.getCommonName();
			this.orgName = subject.getOrganization();

		} catch (IOException e) {
			throw new CertificateReaderException(
					"Failed to read subject [{0}] from certificate into X500Name",
					new Object[] {subjectName}, e);
		}

		String issuerName = this.certificate.getIssuerX500Principal().getName();
		try {
			X500Name issuer = new X500Name(issuerName);
			this.issuer = issuer.getCommonName();
		} catch (IOException e) {
			throw new CertificateReaderException(
					"Failed to read issuer [{0}] from certificate into X500Name",
					new Object[] {issuerName}, e);
		}

		this.serialNumber = this.certificate.getSerialNumber().toString(16);
		this.validFrom = this.certificate.getNotBefore();
		this.validUntil = this.certificate.getNotAfter();

	}

	private String	commonName;
	private String	orgName;
	private String	issuer;
	private String	serialNumber;
	private Date	validFrom;
	private Date	validUntil;

	private X509Certificate	certificate;

	public String getCommonName() {
		return commonName;
	}

	public void setCommonName(String commonName) {
		this.commonName = commonName;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getIssuer() {
		return issuer;
	}

	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidUntil() {
		return validUntil;
	}

	public void setValidUntil(Date validUntil) {
		this.validUntil = validUntil;
	}

	public X509Certificate getCertificate() {
		return certificate;
	}

	@Override
	public String toString() {
		return "CertificateDetails [commonName=" + commonName + ", orgName=" + orgName + ", issuer=" + issuer
				+ ", serialNumber=" + serialNumber + ", validFrom=" + validFrom + ", validUntil=" + validUntil + "]";
	}

}
