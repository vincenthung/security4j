package com.gitlab.vincenthung.security.key;

import java.text.MessageFormat;

public class KeyGeneratorException extends Exception {

	private static final long serialVersionUID = 3748484886042920885L;

	public KeyGeneratorException() {
		super();
	}

	public KeyGeneratorException(Throwable cause) {
		super(cause);
	}

	public KeyGeneratorException(String message, Object[] params, Throwable cause) {
		super(MessageFormat.format(message, params), cause);
	}

	public KeyGeneratorException(String message, Object... params) {
		this(message, params, null);
	}

	public KeyGeneratorException(String message, Throwable cause) {
		this(message, null, cause);
	}

}
