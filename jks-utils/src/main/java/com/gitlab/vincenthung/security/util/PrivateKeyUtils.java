package com.gitlab.vincenthung.security.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;

public class PrivateKeyUtils {

	public static PrivateKey loadPrivateKey(InputStream is, String type)
			throws NoSuchAlgorithmException, IOException, InvalidKeySpecException {

		KeyFactory keyFactory = KeyFactory.getInstance(type);

		byte[] rawBytes = readInputStreamToBytes(is);
		byte[] decodedKey = Base64.getDecoder().decode(PemUtils.extractPrivateKey(rawBytes));

		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(decodedKey);
		return keyFactory.generatePrivate(keySpec);

	}

	private static byte[] readInputStreamToBytes(InputStream is) throws IOException {

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int nRead;
		byte[] data = new byte[4];
		while ((nRead = is.read(data, 0, data.length)) != -1) {
			baos.write(data, 0, nRead);
		}
		baos.flush();
		return baos.toByteArray();
	}
}
