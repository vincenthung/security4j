package com.gitlab.vincenthung.security.keystore;

import java.util.HashMap;
import java.util.Map;

public enum KeyStoreType {

	JKS("JKS"),
	PKCS12("PKCS12"),;

	private static Map<String, KeyStoreType> typeMap;
	static {
		typeMap = new HashMap<String ,KeyStoreType>();
		for (KeyStoreType t : KeyStoreType.values())
			typeMap.put(t.getType(), t);
	}

	KeyStoreType(String type) {
		this.type = type;
	}

	private String type;

	public String getType() {
		return type;
	}

	public KeyStoreType fromValue(String type) {
		return typeMap.get(type);
	}

}
