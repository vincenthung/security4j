package com.gitlab.vincenthung.security.util;

import java.io.InputStream;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.Collection;

public class CertificateUtils {

	public static Certificate loadCertificate(InputStream is, String type) throws CertificateException {
		CertificateFactory factory = CertificateFactory.getInstance(type);
		return factory.generateCertificate(is);
	}

	public static Certificate[] loadCertificates(InputStream is, String type) throws CertificateException {
		CertificateFactory factory = CertificateFactory.getInstance(type);
		Collection<? extends Certificate> certs = factory.generateCertificates(is);
		return certs == null ? null : certs.toArray(new Certificate[] {});
	}
}
