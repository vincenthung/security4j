package com.gitlab.vincenthung.security.certificate;

import java.util.ArrayList;
import java.util.List;

public class CertificateDNBuilder {

	// Cert Owner Information
	private String serialNumber;
	private String email;
	private String commonName;
	private String title;
	private List<String> orgUnitName;
	private List<String> domainComponent;
	private String organizationName;
	private String street;
	private String localityName;
	private String stateName;
	private String postalCode;
	private String country;

	public CertificateDNBuilder serialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
		return this;
	}

	public CertificateDNBuilder email(String email) {
		this.email = email;
		return this;
	}

	public CertificateDNBuilder commonName(String commonName) {
		this.commonName = commonName;
		return this;
	}

	public CertificateDNBuilder title(String title) {
		this.title = title;
		return this;
	}

	public CertificateDNBuilder orgUnitName(String[] orgUnitName) {
		this.orgUnitName = new ArrayList<String>();
		for (String name : orgUnitName) {
			this.orgUnitName.add(name);
		}
		return this;
	}

	public CertificateDNBuilder orgUnitName(List<String> orgUnitName) {
		this.orgUnitName = new ArrayList<String>();
		this.orgUnitName.addAll(orgUnitName);
		return this;
	}

	public CertificateDNBuilder addOrgUnitName(String orgUnitName) {
		if (this.orgUnitName == null)
			this.orgUnitName = new ArrayList<String>();
		this.orgUnitName.add(orgUnitName);
		return this;
	}

	public CertificateDNBuilder domainComponent(String[] domainComponent) {
		this.domainComponent = new ArrayList<String>();
		for (String domain : domainComponent) {
			this.domainComponent.add(domain);
		}
		return this;
	}

	public CertificateDNBuilder domainComponent(List<String> domainComponent) {
		this.domainComponent = new ArrayList<String>();
		this.domainComponent.addAll(domainComponent);
		return this;
	}

	public CertificateDNBuilder addDomainComponent(String domainComponent) {
		if (this.domainComponent == null)
			this.domainComponent = new ArrayList<String>();
		this.domainComponent.add(domainComponent);
		return this;
	}

	public CertificateDNBuilder organizationName(String organizationName) {
		this.organizationName = organizationName;
		return this;
	}

	public CertificateDNBuilder street(String street) {
		this.street = street;
		return this;
	}

	public CertificateDNBuilder localityName(String localityName) {
		this.localityName = localityName;
		return this;
	}

	public CertificateDNBuilder stateName(String stateName) {
		this.stateName = stateName;
		return this;
	}

	public CertificateDNBuilder postalCode(String postalCode) {
		this.postalCode = postalCode;
		return this;
	}

	public CertificateDNBuilder country(String country) {
		this.country = country;
		return this;
	}

	private void append(StringBuilder sb, String key, String name) {
		if (sb.length() > 0)
			sb.append(',');
		sb.append(key).append('=').append('"').append(name).append('"');
	}

	public String toDistinguishedName() {

		StringBuilder sb = new StringBuilder();

		if (serialNumber != null && !serialNumber.isEmpty())
			append(sb, "SERIALNUMBER", serialNumber);

		if (email != null && !email.isEmpty())
			append(sb, "MAIL", email);

		if (commonName != null && !commonName.isEmpty())
			append(sb, "CN", commonName);

		if (title != null && !title.isEmpty())
			append(sb, "T", title);

		if (orgUnitName != null && !orgUnitName.isEmpty())
			for (String name : orgUnitName)
				append(sb, "OU", name);

		if (domainComponent != null && !domainComponent.isEmpty())
			for (String domain : domainComponent)
				append(sb, "DC", domain);

		if (organizationName != null && !organizationName.isEmpty())
			append(sb, "O", organizationName);

		if (street != null && !street.isEmpty())
			append(sb, "STREET", street);

		if (localityName != null && !localityName.isEmpty())
			append(sb, "L", localityName);

		if (stateName != null && !stateName.isEmpty())
			append(sb, "S", stateName);

		if (postalCode != null && !postalCode.isEmpty())
			append(sb, "PC", postalCode);

		if (country != null && !country.isEmpty())
			append(sb, "C", country);

		return sb.toString();
	}

	@Override
	public String toString() {
		return toDistinguishedName();
	}

}
