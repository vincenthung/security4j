package com.gitlab.vincenthung.security.keystore.factory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.KeyStore;
import java.security.KeyStore.Entry;
import java.security.KeyStore.PasswordProtection;
import java.security.KeyStore.PrivateKeyEntry;
import java.security.KeyStore.SecretKeyEntry;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableEntryException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import com.gitlab.vincenthung.security.keystore.CertificateType;
import com.gitlab.vincenthung.security.keystore.KeyStoreType;
import com.gitlab.vincenthung.security.util.CertificateUtils;
import com.gitlab.vincenthung.security.util.PrivateKeyUtils;

public class KeyStoreFactory {

	protected static final Logger LOGGER = Logger.getLogger(KeyStoreFactory.class.getName());

	public static final KeyStoreType DEFAULT_KEY_STORE_TYPE = KeyStoreType.JKS;

	public static final String DEFAULT_PRIVATE_KEY_TYPE = "RSA";

	private KeyStore keyStore;
	private String keyStoreType;
	private char[] keyStorePassword;
	private URL outputUrl;

	protected KeyStoreFactory(KeyStore keyStore, char[] keyStorePassword, String keyStoreType,
			URL outputUrl) {
		this.keyStore = keyStore;
		this.keyStorePassword = keyStorePassword;
		this.keyStoreType = keyStoreType;
		this.outputUrl = outputUrl;
	}

	public static KeyStoreFactory init(URL keyStoreUrl, char[] password, String type)
			throws KeyStoreFactoryException {
		if (type == null || type.length() == 0)
			type = DEFAULT_KEY_STORE_TYPE.getType();

		KeyStore keyStore;
		try {
			keyStore = KeyStore.getInstance(type);
		} catch (KeyStoreException e) {
			throw new KeyStoreFactoryException("No providers support KeyStoreSpi with type - [{0}]",
					new Object[] {type}, e);
		}

		InputStream is = null;
		try {
			if (keyStoreUrl != null)
				is = keyStoreUrl.openStream();
			keyStore.load(is, password);
		} catch (IOException e) {
			throw new KeyStoreFactoryException("Unable to open stream of URL - [{0}]",
					new Object[] {keyStoreUrl}, e);
		} catch (CertificateException | NoSuchAlgorithmException ce) {
			throw new KeyStoreFactoryException(
					"Unable to load the keystore with URL - [{0}], password - [{1}]",
					new Object[] {keyStoreUrl, String.copyValueOf(password)}, ce);
		} finally {
			try { is.close(); } catch (Exception e) {}
		}

		return new KeyStoreFactory(keyStore, password, type, keyStoreUrl);

	}

	public void addTrustedCertificate(URL certUrl, String alias, boolean forceReplaceExisting)
			throws KeyStoreFactoryException {

		InputStream is = null;
		try {
			is = certUrl.openStream();

			try {
				this.addTrustedCertificate(is, alias, forceReplaceExisting);
			} catch (KeyStoreFactoryException e) {
				throw new KeyStoreFactoryException(
						e.getMessage() + " of URL - [{0}]",
						new Object[] {certUrl}, e.getCause());
			}

		} catch (IOException e) {
			throw new KeyStoreFactoryException("Unable to open stream of URL - [{0}]",
					new Object[] {certUrl}, e);
		} finally {
			try { is.close(); } catch (Exception e) {}
		}
	}

	public void addTrustedCertificate(InputStream is, String alias, boolean forceReplaceExisting)
			throws KeyStoreFactoryException {

		String type = CertificateType.X509.getType();

		try {
			Certificate cert = CertificateUtils.loadCertificate(is, type);
			this.addTrustedCertificate(cert, alias, forceReplaceExisting);
		} catch (CertificateException e) {
			throw new KeyStoreFactoryException(
					"Failed to load Certificate with type [{0}]",
					new Object[] {type}, e);
		}

	}

	public void addTrustedCertificate(Certificate cert, String alias, boolean forceReplaceExisting)
			throws KeyStoreFactoryException {

		try {
			if (keyStore.containsAlias(alias)) {
				if (forceReplaceExisting) {
					this.removeAlias(alias);
				} else
					throw new KeyStoreFactoryException("Found existing alias [{0}] in the keystore", alias);
			}

		} catch (KeyStoreException e) {
			throw new KeyStoreFactoryException(
					"Exception found when checking if the alias is in keystore", e);
		}

		try {
			keyStore.setCertificateEntry(alias, cert);
		} catch (KeyStoreException e) {
			throw new KeyStoreFactoryException(
					"Exception found when importing the certificate in keystore", e);
		}
	}

	public void addPrivateKeyWithCertificate(URL privateKeyUrl, URL certificatesUrl, String keyType,
			String alias, char[] keyPassword, boolean forceReplaceExisting)
					throws KeyStoreFactoryException {

		if (keyType == null || keyType.isEmpty())
			keyType = DEFAULT_PRIVATE_KEY_TYPE;

		InputStream privateKeyIs = null;
		InputStream certificatesIs = null;

		try {
			privateKeyIs = privateKeyUrl.openStream();
			try {
				certificatesIs = certificatesUrl.openStream();
				this.addPrivateKeyWithCertificate(
						privateKeyIs, certificatesIs, keyType,
						alias, keyPassword, forceReplaceExisting);
			} catch (IOException e) {
				throw new KeyStoreFactoryException("Unable to open stream of URL - [{0}]",
						new Object[] {certificatesUrl}, e);
			} finally {
				try { certificatesIs.close(); } catch (Exception e) {}
			}

		} catch (IOException e) {
			throw new KeyStoreFactoryException("Unable to open stream of URL - [{0}]",
					new Object[] {privateKeyUrl}, e);
		} finally {
			try { privateKeyIs.close(); } catch (Exception e) {}
		}


	}

	public void addPrivateKeyWithCertificate(InputStream privateKeyStream, InputStream certChainStream,
			String keyType, String alias, char[] keyPassword, boolean forceReplaceExisting)
					throws KeyStoreFactoryException {

		if (keyType == null || keyType.isEmpty())
			keyType = DEFAULT_PRIVATE_KEY_TYPE;

		String certType = CertificateType.X509.getType();

		PrivateKey privateKey;
		Certificate[] certChain;

		try {
			privateKey = PrivateKeyUtils.loadPrivateKey(privateKeyStream, keyType);
		} catch (NoSuchAlgorithmException | InvalidKeySpecException | IOException e) {
			throw new KeyStoreFactoryException("Unable to load private key with type [{0}]",
					new Object[] {keyType}, e);
		}

		try {
			certChain = CertificateUtils.loadCertificates(certChainStream, certType);
		} catch (CertificateException e) {
			throw new KeyStoreFactoryException("Unable to load certificate chain with type [{0}]",
					new Object[] {certType}, e);
		}

		this.addPrivateKeyWithCertificate(privateKey, certChain, alias, keyPassword, forceReplaceExisting);

	}

	public void addPrivateKeyWithCertificate(PrivateKey privateKey, Certificate[] certChain,
			String alias, char[] keyPassword, boolean forceReplaceExisting)
					throws KeyStoreFactoryException {

		try {
			if (keyStore.containsAlias(alias)) {
				if (forceReplaceExisting) {
					this.removeAlias(alias);
				} else
					throw new KeyStoreFactoryException("Found existing alias [{0}] in the keystore", alias);
			}

		} catch (KeyStoreException e) {
			throw new KeyStoreFactoryException(
					"Exception found when checking if the alias is in keystore", e);
		}

		try {
			keyStore.setKeyEntry(alias, privateKey, keyPassword, certChain);
		} catch (KeyStoreException e) {
			throw new KeyStoreFactoryException(
					"Exception found when importing the Private Key with certificates in keystore", e);
		}
	}

	public Certificate getCertificate(String alias) throws KeyStoreFactoryException {
		try {
			return keyStore.getCertificate(alias);
		} catch (KeyStoreException e) {
			throw new KeyStoreFactoryException(
					"Exception found when getting the certificates in keystore", e);
		}
	}

	public <T> T getKeyEntry(String alias, char[] password, Class<T> keyEntryType)
			throws KeyStoreFactoryException {
		Entry entry = getAlias(alias, password);
		if (entry == null)
			return null;

		if (keyEntryType.isInstance(entry))
			return keyEntryType.cast(entry);
		else
			throw new KeyStoreFactoryException(
					"Entry of alias [{0}] with class [{1}] is not an instance of {2}",
					alias, entry.getClass(), keyEntryType.getSimpleName());
	}

	public PrivateKeyEntry getPrivateKeyEntry(String alias, char[] password)
			throws KeyStoreFactoryException {
		return getKeyEntry(alias, password, PrivateKeyEntry.class);
	}

	public SecretKeyEntry getSecretKeyEntry(String alias, char[] password)
			throws KeyStoreFactoryException {
		return getKeyEntry(alias, password, SecretKeyEntry.class);
	}

	public Entry getAlias(String alias, char[] password) throws KeyStoreFactoryException {
		try {
			if (!keyStore.containsAlias(alias))
				return null;
		} catch (KeyStoreException e) {
			throw new KeyStoreFactoryException(
					"Exception found when checking the alias in keystore", e);
		}

		PasswordProtection protection = null;
		if (password != null)
			protection = new PasswordProtection(password);

		try {
			return keyStore.getEntry(alias, protection);
		} catch (NoSuchAlgorithmException | KeyStoreException e) {
			throw new KeyStoreFactoryException(
					"Exception found when getting the Entry in keystore", e);
		} catch (UnrecoverableEntryException e) {
			throw new KeyStoreFactoryException(
					"Incorrect password / not supported for password protected", e);
		}
	}

	public boolean removeAlias(String alias) throws KeyStoreFactoryException {

		try {
			if (keyStore.containsAlias(alias)) {
				keyStore.deleteEntry(alias);
				LOGGER.log(Level.INFO, "Removed existing entry with alias " + alias);
				return true;
			}
		} catch (KeyStoreException e) {
			throw new KeyStoreFactoryException(
					"Exception found when checking / removing the alias in keystore", e);
		}
		return false;
	}

	public KeyManager[] getKeyManagers(char[] keyPassword, String type) throws KeyStoreFactoryException {
		return getKeyManagers(keyStore, keyPassword, type);
	}

	public KeyManager[] getKeyManagers(String keyAlias, char[] password, String type)
			throws KeyStoreFactoryException {
		PrivateKeyEntry privateKeyEntry = getPrivateKeyEntry(keyAlias, password);
		KeyStoreFactory keyStoreFactory = KeyStoreFactory.init(null, null, null);
		keyStoreFactory.addPrivateKeyWithCertificate(
				privateKeyEntry.getPrivateKey(),
				privateKeyEntry.getCertificateChain(),
				keyAlias, password, true);
		return getKeyManagers(keyStoreFactory.getKeyStore(), password, type);
	}

	protected KeyManager[] getKeyManagers(KeyStore keyStore, char[] password, String type)
			throws KeyStoreFactoryException {

		if (type == null || type.isEmpty())
			type = KeyManagerFactory.getDefaultAlgorithm();

		KeyManagerFactory keyManagerFactory;
		try {
			keyManagerFactory = KeyManagerFactory.getInstance(type);
		} catch (NoSuchAlgorithmException | NullPointerException e) {
			throw new KeyStoreFactoryException(
					"No providers support KeyManagerFactorySpi with type - [{0}]",
					new Object[] {type}, e);
		}
		try {
			keyManagerFactory.init(keyStore, password);
		} catch (UnrecoverableKeyException e) {
			throw new KeyStoreFactoryException(
					"Incorrect password / not supported for password protected", e);
		} catch (KeyStoreException | NoSuchAlgorithmException e) {
			throw new KeyStoreFactoryException("Unable to init the keyManagerFactory", e);
		}
		return keyManagerFactory.getKeyManagers();
	}

	public void save() throws KeyStoreFactoryException {
		if (outputUrl == null)
			throw new KeyStoreFactoryException(
					"There are no corresponding KeyStore File, please call method export instead");

		URI outputUri;
		try {
			outputUri = outputUrl.toURI();
		} catch (URISyntaxException e) {
			throw new KeyStoreFactoryException(
					"Exception found when converting URL [{0}] to uri",	new Object[] {outputUrl}, e);
		}

		File keyStoreFile;
		try {
			keyStoreFile = new File(outputUri);
		} catch (IllegalArgumentException e) {
			throw new KeyStoreFactoryException(
					"Exception found when creating File object with URI [{0}]",
					new Object[] {outputUri}, e);
		}

		File keyStoreFileBak = new File(keyStoreFile.getAbsoluteFile() + ".bak");
		File keyStoreFileTmp = new File(keyStoreFile.getAbsoluteFile() + ".tmp");

		if (keyStoreFileTmp.exists())
			keyStoreFileTmp.delete();

		if (keyStoreFileBak.exists())
			if (!keyStoreFileBak.renameTo(keyStoreFileTmp))
				throw new KeyStoreFactoryException(
						"Failed to move the KeyStore backup file to temp path [{0}]",
						keyStoreFileTmp.getAbsolutePath());

		if (keyStoreFile.exists())
			if (!keyStoreFile.renameTo(keyStoreFileBak))
				throw new KeyStoreFactoryException(
						"Failed to move the KeyStore file to backup path [{0}]",
						keyStoreFileBak.getAbsolutePath());

		try {
			this.export(keyStoreFile, false);
		} catch (KeyStoreFactoryException e) {

			if (keyStoreFileBak.exists())
				if (keyStoreFileBak.renameTo(keyStoreFile))
					LOGGER.log(Level.INFO, "Restored KeyStore from backup");
				else
					LOGGER.log(Level.WARNING, "Failed to restore KeyStore from backup");

			if (keyStoreFileTmp.exists())
				if (keyStoreFileTmp.renameTo(keyStoreFileBak))
					LOGGER.log(Level.INFO, "Restored KeyStore backup from temp");
				else
					LOGGER.log(Level.WARNING, "Failed to restore KeyStore backup from temp");

			throw e;
		}

		keyStoreFileTmp.delete();

	}

	public void export(File outputFile, boolean updateOutputUrl) throws KeyStoreFactoryException {

		if (outputFile.exists())
			throw new KeyStoreFactoryException("Found existing file [{0}] when exporting the KeyStore",
					outputFile.getAbsolutePath());

		FileOutputStream fos = null;
		try {
			outputFile.getAbsoluteFile().getParentFile().mkdirs();
			fos = new FileOutputStream(outputFile);
			keyStore.store(fos, keyStorePassword);
			if (updateOutputUrl)
				this.outputUrl = outputFile.toURI().toURL();
		} catch (FileNotFoundException e) {
			throw new KeyStoreFactoryException(
					"Failed to create FileOutputStream of the path [{0}]",
					new Object[] {outputFile.getAbsolutePath()}, e);
		} catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException e) {
			throw new KeyStoreFactoryException(
					"Failed to export KeyStore into file with path [{0}]",
					new Object[] {outputFile.getAbsolutePath()}, e);
		} finally {
			try { fos.close(); } catch (Exception e) {}
		}
	}

	public TrustManager[] getTrustManagers(List<String> aliases) throws KeyStoreFactoryException {
		if (aliases == null || aliases.isEmpty())
			return null;

		KeyStoreFactory keyStoreFactory = KeyStoreFactory.init(null, null, null);
		for (String alias : aliases) {
			Certificate certificate = this.getCertificate(alias);
			if (certificate != null)
				keyStoreFactory.addTrustedCertificate(certificate, alias, true);
		}
		return new TrustManager[] { keyStoreFactory.getTrustManager() };
	}

	public X509TrustManager getTrustManager() throws KeyStoreFactoryException {
		TrustManagerFactory factory;
		try {
			factory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		} catch (NoSuchAlgorithmException e) {
			throw new KeyStoreFactoryException(
					"No providers support TrustManagerFactorySpi with type - [{0}]",
					new Object[] {TrustManagerFactory.getDefaultAlgorithm()}, e);
		}
		try {
			factory.init(keyStore);
		} catch (KeyStoreException e) {
			throw new KeyStoreFactoryException("Failed to initialize TrustManagerFactory", e);
		}
		for (TrustManager trustManager : factory.getTrustManagers())
			if (trustManager instanceof X509TrustManager)
				return (X509TrustManager) trustManager;

		throw new KeyStoreFactoryException("No TrustManager of class X509TrustManager have been created");

	}

	public void changeKeyStorePassword(char[] newPassword) {
		this.keyStorePassword = newPassword;
	}

	public KeyStore getKeyStore() {
		return this.keyStore;
	}

	public String getKeyStoreType() {
		return this.keyStoreType;
	}

}
