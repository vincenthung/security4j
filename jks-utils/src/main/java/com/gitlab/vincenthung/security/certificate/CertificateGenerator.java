package com.gitlab.vincenthung.security.certificate;

import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore.PrivateKeyEntry;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.Date;

import sun.security.x509.AlgorithmId;
import sun.security.x509.AuthorityKeyIdentifierExtension;
import sun.security.x509.CertificateAlgorithmId;
import sun.security.x509.CertificateExtensions;
import sun.security.x509.CertificateSerialNumber;
import sun.security.x509.CertificateValidity;
import sun.security.x509.CertificateVersion;
import sun.security.x509.CertificateX509Key;
import sun.security.x509.KeyIdentifier;
import sun.security.x509.SubjectKeyIdentifierExtension;
import sun.security.x509.X500Name;
import sun.security.x509.X509CertImpl;
import sun.security.x509.X509CertInfo;

public class CertificateGenerator {

	private static final int CERTIFICATE_VERSION = CertificateVersion.V3;

	private String secureRandomAlgo;
	private String privateKeyType;
	private int privateKeySize;
	private String signatureAlgorithm;

	private String certOwnerDN;
	private CertificateValidity interval;

	public CertificateGenerator(String privateKeyType, int privateKeySize,
			String secureRandomAlgo, String signatureAlgorithm,
			String certOwnerDN, CertificateValidity interval) {
		this.privateKeyType = privateKeyType;
		this.privateKeySize = privateKeySize;
		this.secureRandomAlgo = secureRandomAlgo;
		this.signatureAlgorithm = signatureAlgorithm;
		this.certOwnerDN = certOwnerDN;
		this.interval = interval;
	}

	public KeyPair generateKeyPair() throws CertificateGeneratorException {

		KeyPairGenerator keyPairGenerator;
		try {
			keyPairGenerator = KeyPairGenerator.getInstance(privateKeyType);
		} catch (NoSuchAlgorithmException e) {
			throw new CertificateGeneratorException(
					"No providers support KeyPairGeneratoreSpi with type - [{0}]",
					new Object[] {privateKeyType}, e);
		}

		try {
			keyPairGenerator.initialize(privateKeySize, SecureRandom.getInstance(secureRandomAlgo));
		} catch (NoSuchAlgorithmException e) {
			throw new CertificateGeneratorException("No providers support SecureRandomSpi with type - [{0}]",
					new Object[] {secureRandomAlgo}, e);
		}

		return keyPairGenerator.generateKeyPair();
	}

	public PrivateKeyEntry generateSelfSignedCertificate() throws CertificateGeneratorException {
		return generateSignedCertificate(null);
	}

	public PrivateKeyEntry generateSignedCertificate(PrivateKeyEntry issuerCertEntry)
			throws CertificateGeneratorException {
		return generateSignedCertificate(generateKeyPair(), issuerCertEntry, signatureAlgorithm);
	}

	public PrivateKeyEntry generateSignedCertificate(KeyPair certKeyPair, PrivateKeyEntry issuerCertEntry,
			String signatureAlgorithm) throws CertificateGeneratorException {

		X500Name certSubjectName;
		try {
			certSubjectName = new X500Name(certOwnerDN);
		} catch (IOException e) {
			throw new CertificateGeneratorException("Failed to create owner X500Name with DN [{0}]",
					new Object[] {certOwnerDN}, e);
		}

		X500Name certIssuer;
		Certificate[] issuerCertChain;
		PrivateKey issuerPrivateKey;

		// Initialize the extension
		CertificateExtensions extensions = new CertificateExtensions();
		try {
			extensions.set(SubjectKeyIdentifierExtension.NAME, new SubjectKeyIdentifierExtension(
					new KeyIdentifier(certKeyPair.getPublic()).getIdentifier()));
		} catch (IOException e) {
			throw new CertificateGeneratorException(
					"Failed to set SubjectKeyIdentifier into Extension", e);
		}

		if (issuerCertEntry == null) {
			certIssuer = certSubjectName;
			issuerCertChain = new Certificate[] {};
			issuerPrivateKey = certKeyPair.getPrivate();
		} else {
			X509Certificate issuerCert = (X509Certificate) issuerCertEntry.getCertificate();
			String issuerDN = issuerCert.getSubjectDN().getName();
			try {
				certIssuer = new X500Name(issuerDN);
			} catch (IOException e) {
				throw new CertificateGeneratorException("Failed to create issuer X500Name with DN [{0}]",
						new Object[] {issuerDN}, e);
			}
			issuerCertChain = issuerCertEntry.getCertificateChain();
			issuerPrivateKey = issuerCertEntry.getPrivateKey();

			// Set the AuthorityKeyIdentifier in Extension
			try {
				extensions.set(AuthorityKeyIdentifierExtension.NAME, new AuthorityKeyIdentifierExtension(
						new KeyIdentifier(issuerCert.getPublicKey()), null, null));
			} catch (IOException e) {
				throw new CertificateGeneratorException(
						"Failed to set AuthorityKeyIdentifier into Extension", e);
			}

		}

		X509CertInfo certInfo = new X509CertInfo();

		try {
			certInfo.set(X509CertInfo.VALIDITY, interval);
		} catch (CertificateException | IOException e) {
			throw new CertificateGeneratorException("Incompatible CertificateValidity - [{0}]",
					new Object[] {interval}, e);
		}
		try {
			certInfo.set(X509CertInfo.SUBJECT, certSubjectName);
		} catch (CertificateException | IOException e) {
			throw new CertificateGeneratorException(
					"Incompatible Subject X500Name - [{0}]",
					new Object[] {certSubjectName}, e);
		}
		try {
			certInfo.set(X509CertInfo.ISSUER, certIssuer);
		} catch (CertificateException | IOException e) {
			throw new CertificateGeneratorException(
					"Incompatible Issuer X500Name - [{0}]",
					new Object[] {certIssuer}, e);
		}
		try {
			certInfo.set(X509CertInfo.SERIAL_NUMBER, new CertificateSerialNumber(generateSerialNumber()));
		} catch (CertificateException | IOException e) {
			throw new CertificateGeneratorException(
					"Incompatible CertificateSerialNumber(BigDecimal)",
					null, e);
		}
		try {
			certInfo.set(X509CertInfo.KEY, new CertificateX509Key(certKeyPair.getPublic()));
		} catch (CertificateException | IOException e) {
			throw new CertificateGeneratorException(
					"Incompatible CertificateX509Key(PublicKey) - [{0}]",
					new Object[] {certKeyPair.getPublic()}, e);
		}
		try {
			certInfo.set(X509CertInfo.VERSION, new CertificateVersion(CERTIFICATE_VERSION));
		} catch (CertificateException | IOException e) {
			throw new CertificateGeneratorException(
					"Incompatible CertificateVersion - [{0}]",
					new Object[] {CERTIFICATE_VERSION}, e);
		}
		try {
			certInfo.set(X509CertInfo.ALGORITHM_ID,
					new CertificateAlgorithmId(AlgorithmId.get(signatureAlgorithm)));
		} catch (NoSuchAlgorithmException e) {
			throw new CertificateGeneratorException(
					"No AlgorithmId found for the algorithm - [{0}]",
					new Object[] {signatureAlgorithm}, e);
		} catch (CertificateException | IOException e) {
			throw new CertificateGeneratorException(
					"Incompatible CertificateAlgorithmId(AlgorithmId) - [{0}]",
					new Object[] {signatureAlgorithm}, e);
		}

		X509CertImpl cert = new X509CertImpl(certInfo);
		try {
			cert.sign(issuerPrivateKey, signatureAlgorithm);
		} catch (InvalidKeyException | CertificateException | NoSuchAlgorithmException
				| NoSuchProviderException | SignatureException e) {
			throw new CertificateGeneratorException(
					"Failed when signing the certificate with algorithm [{0}]",
					new Object[] {signatureAlgorithm}, e);
		}

		// Update the algorithm
		try {
			certInfo.set(CertificateAlgorithmId.NAME + "." + CertificateAlgorithmId.ALGORITHM,
					(AlgorithmId) cert.get(X509CertImpl.SIG_ALG));
		} catch (CertificateException | IOException e) {
			Object algo = "CertificateParsingException";
			try { algo = cert.get(X509CertImpl.SIG_ALG); } catch (CertificateParsingException cpe) {}
			throw new CertificateGeneratorException(
					"Incompatible algorithmID.algorithm - [{0}]",
					new Object[] {algo}, e);
		}

		try {
			certInfo.set(X509CertInfo.EXTENSIONS, extensions);
		} catch (CertificateException | IOException e) {
			throw new CertificateGeneratorException("Incompatible CertificateExtensions - [{0}]",
					new Object[] {extensions}, e);
		}

		// Resign the certificate
		cert = new X509CertImpl(certInfo);
		try {
			cert.sign(issuerPrivateKey, signatureAlgorithm);
		} catch (InvalidKeyException | CertificateException | NoSuchAlgorithmException
				| NoSuchProviderException | SignatureException e) {
			throw new CertificateGeneratorException(
					"Failed when signing the certificate with algorithm [{0}]",
					new Object[] {signatureAlgorithm}, e);
		}

		Certificate[] certChain = new Certificate[issuerCertChain.length + 1];
		certChain[0] = cert;
		System.arraycopy(issuerCertChain, 0, certChain, 1, issuerCertChain.length);

		return new PrivateKeyEntry(certKeyPair.getPrivate(), certChain);
	}

	protected BigInteger generateSerialNumber() {
		return new BigInteger(64, new SecureRandom());
	}

	public String getSecureRandomAlgo() {
		return secureRandomAlgo;
	}

	public String getPrivateKeyType() {
		return privateKeyType;
	}

	public int getPrivateKeySize() {
		return privateKeySize;
	}

	public String getSignatureAlgorithm() {
		return signatureAlgorithm;
	}

	public String getCertOwnerDN() {
		return certOwnerDN;
	}

	public Date getValidFrom() {
		try {
			return interval.get(CertificateValidity.NOT_BEFORE);
		} catch (IOException e) {
			return null;
		}
	}

	public Date getValidTo() {
		try {
			return interval.get(CertificateValidity.NOT_AFTER);
		} catch (IOException e) {
			return null;
		}
	}

}
