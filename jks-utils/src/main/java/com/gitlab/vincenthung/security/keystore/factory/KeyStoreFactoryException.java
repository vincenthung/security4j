package com.gitlab.vincenthung.security.keystore.factory;

import java.text.MessageFormat;

public class KeyStoreFactoryException extends Exception {

	private static final long serialVersionUID = 2210867787284577949L;

	public KeyStoreFactoryException() {
		super();
	}

	public KeyStoreFactoryException(Throwable cause) {
		super(cause);
	}

	public KeyStoreFactoryException(String message, Object[] params, Throwable cause) {
		super(MessageFormat.format(message, params), cause);
	}

	public KeyStoreFactoryException(String message, Object... params) {
		this(message, params, null);
	}

	public KeyStoreFactoryException(String message, Throwable cause) {
		this(message, null, cause);
	}

}
