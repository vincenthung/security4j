package com.gitlab.vincenthung.security.certificate;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import sun.security.x509.CertificateValidity;

public class CertificateGeneratorBuilder {

	public static final String	DEFAULT_PRIVATE_KEY_TYPE	= "RSA";
	public static final int		DEFAULT_PRIVATE_KEY_SIZE	= 1024;
	public static final String	DEFAULT_SECURE_RANDOM_ALGO	= "SHA1PRNG";

	public static final String	DEFAULT_SIGNATURE_ALGO		= "sha256WithRSA";
	public static final int		DEFAULT_CERT_VALIDITY_DAYS	= 3650;

	public static final String	DEFAULT_CERT_OWNER_DN		= "CN=Undefined";

	private String	privateKeyType;
	private int		privateKeySize;
	private String	secureRandomAlgo;

	private String	signatureAlgo;
	private String	certOwnerDN;

	private int		validityDays;
	private Date	validFrom;
	private Date	validTo;


	public CertificateGenerator build() {

		String privateKeyType = this.privateKeyType;
		if (privateKeyType == null || privateKeyType.isEmpty())
			privateKeyType = DEFAULT_PRIVATE_KEY_TYPE;

		int privateKeySize = this.privateKeySize;
		if (privateKeySize <= 0)
			privateKeySize = DEFAULT_PRIVATE_KEY_SIZE;

		String secureRandomAlgo = this.secureRandomAlgo;
		if (secureRandomAlgo == null || secureRandomAlgo.isEmpty())
			secureRandomAlgo = DEFAULT_SECURE_RANDOM_ALGO;

		String signatureAlgo = this.signatureAlgo;
		if (signatureAlgo == null || signatureAlgo.isEmpty())
			signatureAlgo = DEFAULT_SIGNATURE_ALGO;

		String certOwnerDN = this.certOwnerDN;
		if (certOwnerDN == null || certOwnerDN.isEmpty())
			certOwnerDN = DEFAULT_CERT_OWNER_DN;

		Date validFrom	= this.validFrom == null ? new Date() : this.validFrom;
		Date validTo;
		if (this.validTo == null) {
			int validityDays = this.validityDays;
			if (validityDays <= 0)
				validityDays = DEFAULT_CERT_VALIDITY_DAYS;
			validTo = new Date(validFrom.getTime() + TimeUnit.DAYS.toMillis(validityDays));
		} else
			validTo = this.validTo;

		CertificateValidity validity = new CertificateValidity(validFrom, validTo);

		return new CertificateGenerator(privateKeyType, privateKeySize, secureRandomAlgo,
				signatureAlgo, certOwnerDN, validity);
	}

	public CertificateGeneratorBuilder privateKeyType(String privateKeyType) {
		this.privateKeyType = privateKeyType;
		return this;
	}

	public CertificateGeneratorBuilder privateKeySize(int privateKeySize) {
		this.privateKeySize = privateKeySize;
		return this;
	}

	public CertificateGeneratorBuilder secureRandomAlgo(String secureRandomAlgo) {
		this.secureRandomAlgo = secureRandomAlgo;
		return this;
	}

	public CertificateGeneratorBuilder signatureAlgo(String signatureAlgo) {
		this.signatureAlgo = signatureAlgo;
		return this;
	}

	public CertificateGeneratorBuilder certOwnerDN(String certOwnerDN) {
		this.certOwnerDN = certOwnerDN;
		return this;
	}

	public CertificateGeneratorBuilder validityDays(int validityDays) {
		this.validityDays = validityDays;
		return this;
	}

	public CertificateGeneratorBuilder validFrom(Date validFrom) {
		this.validFrom = validFrom;
		return this;
	}

	public CertificateGeneratorBuilder validTo(Date validTo) {
		this.validTo = validTo;
		return this;
	}
}
