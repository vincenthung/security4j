package com.gitlab.vincenthung.security.util;

public class PemUtils {

	public static byte[] extractPrivateKey(byte[] bytes) {
		return new String(bytes)
				.replace("-----BEGIN PRIVATE KEY-----", "")
				.replaceAll("\r?\n", "")
				.replace("-----END PRIVATE KEY-----", "")
				.getBytes();
	}

	public static byte[] extractCertificate(byte[] bytes) {
		return new String(bytes)
				.replace("-----BEGIN CERTIFICATE-----", "")
				.replaceAll("\r?\n", "")
				.replace("-----END CERTIFICATE-----", "")
				.getBytes();
	}

}
