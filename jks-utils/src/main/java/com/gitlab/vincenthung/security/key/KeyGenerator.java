package com.gitlab.vincenthung.security.key;

import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class KeyGenerator {

	public static final String	DEFAULT_SYMMETRIC_KEY_TYPE = "AES";
	public static final int		DEFAULT_SYMMETRIC_KEY_SIZE = 128;

	public static SecretKey generateSymmetricKey(String type, int keySize) throws KeyGeneratorException {
		if (type == null || type.isEmpty())
			type = DEFAULT_SYMMETRIC_KEY_TYPE;

		if (keySize <= 0)
			keySize = DEFAULT_SYMMETRIC_KEY_SIZE;

		javax.crypto.KeyGenerator generator;
		try {
			generator = javax.crypto.KeyGenerator.getInstance(type);
		} catch (NoSuchAlgorithmException e) {
			throw new KeyGeneratorException(
					"No providers support KeyGeneratoreSpi with type - [{0}]",
					new Object[] {type}, e);
		}
		generator.init(keySize);
		return generator.generateKey();
	}

	public static SecretKey loadSecretKey(byte[] keyBytes, String type) {
		return new SecretKeySpec(keyBytes, type);
	}

	public static SecretKey loadEncodedSecretKey(byte[] encodedKeyBytes, String type) {
		return loadSecretKey(Base64.getDecoder().decode(encodedKeyBytes), type);
	}

	public static SecretKey loadEncodedSecretKey(String encodedKey, String type) {
		return loadSecretKey(Base64.getDecoder().decode(encodedKey), type);
	}
}
