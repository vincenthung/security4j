package com.gitlab.vincenthung.security.keystore;

import java.util.HashMap;
import java.util.Map;

public enum CertificateType {

	X509("X.509"),;

	private static Map<String, CertificateType> typeMap;
	static {
		typeMap = new HashMap<String ,CertificateType>();
		for (CertificateType t : CertificateType.values())
			typeMap.put(t.getType(), t);
	}

	CertificateType(String type) {
		this.type = type;
	}

	private String type;

	public String getType() {
		return type;
	}

	public CertificateType fromValue(String type) {
		return typeMap.get(type);
	}

}
